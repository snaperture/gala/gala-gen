﻿using CommandLine;

namespace Gala
{
	class Options
	{
		[Option('v', "verbose", Default = false)]
		public bool Verbose { get; set; }

		[Option('m', "media-root", Required = true, HelpText = "Location of the media files to index")]
		public string MediaRoot { get; set; }

		[Option('t', "title", Default = "Gallery", HelpText = "Title of the generated gallery site")]
		public string GalleryTitle { get; set; }
	}
}
