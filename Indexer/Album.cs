﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Gala.Indexer
{
	public class Album : Entity
	{
		public readonly List<Album> SubAlbums = new List<Album>();
		public readonly List<Image> Images = new List<Image>();

		public Album(string path, Album parent = null) : base(path, parent) {

		}

		public override string Name {
			get {
				if (Parent == null) {
					return "Gallery Root";
				} else {
					return Path.GetFileName(FilePath);
				}
			}
		}

		public void Add(Album album) {
			SubAlbums.Add(album);
		}

		public void Add(Image image) {
			Images.Add(image);
		}

		public string Tree() {
			return Tree(0);
		}

		string Tree(int level) {
			StringBuilder result = new StringBuilder();

			result.Append(new string('\t', level));
			result.Append("+ ");
			result.AppendLine(Name);

			foreach (var image in Images) {
				result.Append(new string('\t', level + 1));
				result.Append("- ");
				result.AppendLine(image.Name);
			}

			foreach (var subAlbum in SubAlbums) {
				result.Append(subAlbum.Tree(level + 1));
			}

			return result.ToString();
		}
	}
}
