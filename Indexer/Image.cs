﻿using MetadataExtractor;
using MetadataExtractor.Formats.Exif;
using MetadataExtractor.Formats.Iptc;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace Gala.Indexer
{
	public class Image : Entity
	{
		private readonly int[] ExtractExifTags = {
			ExifDirectoryBase.TagDateTime,
			ExifDirectoryBase.TagExposureTime,
			ExifDirectoryBase.TagFlash,
			ExifDirectoryBase.TagFNumber,
			ExifDirectoryBase.TagLensMake,
			ExifDirectoryBase.TagLensModel,
			ExifDirectoryBase.TagMake,
			ExifDirectoryBase.TagModel,
			ExifDirectoryBase.TagFocalLength,
		};

		readonly IReadOnlyList<MetadataExtractor.Directory> metadata;

		public Image(string path, Album parent) : base(path, parent) {
			metadata = ImageMetadataReader.ReadMetadata(path);
		}

		public override string Name => Path.GetFileNameWithoutExtension(FilePath);

#nullable enable
		ExifSubIfdDirectory? ExifMetadata => metadata.OfType<ExifSubIfdDirectory>().FirstOrDefault();
		IptcDirectory? IptcMetadata => metadata.OfType<IptcDirectory>().FirstOrDefault();

		public ReadOnlyCollection<string>? Keywords {
			get {
				string[]? keywords = IptcMetadata?.GetStringArray(IptcDirectory.TagKeywords);
				return keywords == null ? null : new ReadOnlyCollection<string>(keywords);
			}
		}
#nullable restore

		public ReadOnlyDictionary<string, string> Metadata {
			get {
				var dictionary = new Dictionary<string, string>();

				if (ExifMetadata != null) {
					foreach (var tag in ExtractExifTags) {
						dictionary[ExifMetadata.GetTagName(tag)] = ExifMetadata.GetDescription(tag);
					}
				}

				return new ReadOnlyDictionary<string, string>(dictionary);
			}
		}
	}
}
