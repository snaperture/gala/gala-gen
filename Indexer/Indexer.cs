﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Gala.Indexer
{
	public static class Indexer
	{
		public static Album Index(this string path, Album parent = null) {
			if (!Directory.Exists(path)) {
				throw new DirectoryNotFoundException($"Path '{path}' does not exist and/or is not a directory");
			}

			var album = new Album(path, parent);

			// Index media files
			foreach (var file in Directory.EnumerateFiles(path)) {
				album.Add(new Image(file, parent: album));
			}

			// Enqueue sub-albums for indexing
			var tasks = new List<Task>();

			foreach (var subdir in Directory.EnumerateDirectories(path)) {
				tasks.Add(Task.Run(() => album.Add(subdir.Index(parent: album))));
			}

			Task.WaitAll(tasks.ToArray());
			return album;
		}
	}
}
