﻿namespace Gala.Indexer
{
	public abstract class Entity
	{
		public readonly string FilePath;
		public readonly Album Parent;

		public Entity(string path, Album parent = null) {
			FilePath = path;
			Parent = parent;
		}

		public abstract string Name { get; }

		public override string ToString() {
			return $"{GetType().Name} '{Name}'";
		}

		public override int GetHashCode() {
			return GetType().GetHashCode() ^ FilePath.GetHashCode();
		}

		public override bool Equals(object obj) {
			return obj.GetType() == GetType() && ((Entity) obj).FilePath == FilePath;
		}
	}
}
