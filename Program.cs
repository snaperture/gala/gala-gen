﻿using CommandLine;
using Gala.Indexer;
using System;

namespace Gala
{
	class Program
	{
		static void Main(string[] args) {
			Parser.Default.ParseArguments<Options>(args)
				.WithParsed(RunGalaEngine)
				.WithNotParsed(errors => {
					Environment.Exit(1);
				});
		}

		static void RunGalaEngine(Options options) {
			var rootAlbum = options.MediaRoot.Index();

			if (options.Verbose) {
				Console.WriteLine(rootAlbum.Tree());
			}
		}
	}
}
